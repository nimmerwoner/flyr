import sys
import flyr
import numpy as np
from PIL import Image

print(f"Reading {sys.argv[1]}")
thermogram = flyr.unpack(sys.argv[1])
print(thermogram.celsius)
print("   ^--- thermogram in °C, °K also available\n")

print("Rendering to grayscale.png")
img = thermogram.render_pil(palette="grayscale")
img.save("grayscale.png")

print("Rendering to jet.png")
render = thermogram.render(palette="jet")
img = Image.fromarray(render)
img.save("jet.png")

print("Saving optical image")
optical = thermogram.optical
img = Image.fromarray(optical)
img.save("optical.png")
print(optical)
print("   ^--- photo embedded in thermogram\n")
