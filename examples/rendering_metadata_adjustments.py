import flyr

filepath = "../thermograms/flir_e8xt_1.jpg"
thermogram_095 = flyr.unpack(filepath)  # Embedded emissivity is 0.95

# Simple example
# We render the thermogram in 2 different ways. Once with the embedded emissivity of 0.95
# and once with an adjusted emissivity of 1.00.
# To see the temperature differences, we need to use a temperature range that we want to
# render. The example thermogram used (20.6, 33.8) so we will too.
print("Starting simple example")
img = thermogram_095.render_pil(
    min_v=20.6,  # Important! Impacts which temperatures map to which colors
    max_v=33.8,  # Important! Impacts which temperatures map to which colors
    unit="celsius",  # Our minimum and maximum values are in celsius
    palette="turbo",
)
img.save("emissivity-095-original.png")
print("Created emissivity-095-original.png: Original emissivity / metadata.")

thermogram_100 = thermogram_095.adjust_metadata(emissivity=1.0)  # Update E to 1.00
img = thermogram_100.render_pil(
    # This time we use kelvin to set our minimum and maximum values.
    # We only do this here to show it is possible. Celsius works fine too.
    min_v=20.6 + 273.15,
    max_v=33.8 + 273.15,
    unit="celsius",
    palette="turbo",
)
img.save("emissivity-100-adjusted.png")
print("Created emissivity-100-adjusted.png: Adjusted emissivity / metadata.")


# Advanced example
# Let's render the thermogram with emissivities from 0.1 to 1.0 for several different
# palettes.
print("\nStarting advanced example")
palettes = ["turbo", "inferno", "viridis"]
thermogram = thermogram_095
min_temp = 20.6 + 273.15
max_temp = 33.8 + 273.15
for e_int in range(1, 11):
    e = e_int / 10  # Map integer e (1, 2, 3, ...) to decimal (0.1, 0.2, 0.3, ...)
    for p in palettes:
        o = f"render-{p}-emissivity-{e}.png"
        print(f"Creating {o} for emissivity {e} with palette {p}")
        # This is the interesting part. Adjust the emissivity, then render to the given
        # temperature range
        thermogram = thermogram.adjust_metadata(emissivity=e)
        img = thermogram.render_pil(
            min_v=min_temp, max_v=max_temp, unit="minmax", palette=p
        )
        img.save(o)
