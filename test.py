#!/usr/bin/env python3

import os
import sys
from io import BytesIO
from typing import Tuple, BinaryIO, List, Optional
from urllib.request import urlopen

import flyr
from flyr import FlyrThermogram
import numpy as np
from nptyping import Array
from flirimageextractor import FlirImageExtractor


def go():
    subjects = define_test_subjects(args.flir_samples)
    subjects = [args.thermogram] if args.thermogram else subjects

    for subject in subjects:
        try:
            file_data, file_name = get_subject(subject)
            test(file_data, file_name)
        except Exception as e:
            msg = "\033[1;31mFAILURE\033[0m because {}".format(e)
            print(msg)


def test(file_data: BinaryIO, file_name: str):
    msg_attempting = "Attempting to unpack {}"
    msg_success = "\033[1;32mSUCCESS\033[0m unpacked correct thermal data from {} (greatest difference: {} °C)"
    msg_unequal = "\033[1;33mUNEQUAL\033[0m for {}: Different temperatures! Average difference: {} °C. Greatest difference: {} °C"
    msg_err_verif = "\033[1;31mFAILURE\033[0m for {} verifying thermal data"
    msg_err_unpack = "\033[1;31mFAILURE\033[0m on {} during unpacking"
    msg_err_other = "\033[1;31mFAILURE\033[0m on {} because {}"

    try:
        print(msg_attempting.format(file_name), end="\r")

        file_data.seek(0)
        t_flyr = flyr.unpack(file_data)
        file_data.seek(0)
        t_fie = FlirImageExtractor()
        t_fie.process_image(file_data)

        try:
            success, avg_diff, grt_diff = verify_thermal(t_flyr, t_fie)
            # success, avg_diff, grt_diff = True, 0.0, 0.0

            if success:
                print(msg_success.format(file_name, grt_diff), end="\r")
            else:
                print(msg_unequal.format(file_name, avg_diff, grt_diff), end="\r")
        except Exception as e:
            print(msg_err_verif.format(file_name), end="\r")
            print(e)

    except ValueError:
        print(msg_err_unpack.format(file_name), end="\r")
    except Exception as e:
        print(msg_err_other.format(file_name, e), end="\r")
    print()


def verify_thermal(
    thermogram: FlyrThermogram, extractor: FlirImageExtractor
) -> Tuple[bool, float, float]:
    # Extract with FlirImageExtractor
    thermal_fie = extractor.get_thermal_np()

    # Calculate differences
    thermal_flyr = thermogram.celsius
    count = thermal_flyr.shape[0] * thermal_flyr.shape[1]
    diffs = np.abs(thermal_flyr - thermal_fie)
    avg_diff = diffs.sum() / count

    return (
        np.all(diffs < 0.0005)
        # and test_info(fie.meta, thermogram.camera_info)
        # and np.all(fie.thermal_np_raw == thermogram.raw_data)
        and thermal_flyr.shape == thermal_fie.shape,
        avg_diff,
        diffs.max(),
    )


def get_subject(src: str) -> Tuple[BinaryIO, str]:
    if os.path.isfile(src):
        file_name = src.rsplit("/", 1)[-1]
        file_path = src
    else:
        file_name, file_path = formulate_file_paths(src)

    if os.path.isfile(file_path):
        print("Reading {}".format(file_name), end="\r")
        file_data = read(file_path)
    else:
        print("Downloading {}".format(file_name), end="\r")
        file_data = download(src, file_path)

    return (file_data, file_name)


def formulate_file_paths(url: str) -> Tuple[str, str]:
    file_name = url.rsplit("/", 1)[-1]
    file_path = os.path.join(args.dir, file_name)
    return file_name, file_path


def read(path: str) -> BinaryIO:
    with open(path, "rb") as handle:
        data = handle.read()
    return BytesIO(data)


def download(url: str, dest: str) -> BinaryIO:
    with open(dest, "wb") as handle:
        data = urlopen(url).read()
        handle.write(data)
    return BytesIO(data)


def test_info(a, b) -> bool:
    try:
        assert abs(a["Emissivity"] - b["E"] < 0.01), "Emissivity different"
        assert (
            abs(float(a["SubjectDistance"].split(" ", 1)[0]) - b["OD"]) < 0.01
        ), "Subject distance different"
        assert (
            abs(float(a["AtmosphericTemperature"].split(" ", 1)[0]) - b["ATemp"]) < 0.01
        ), "Atmospheric temperature different"
        assert (
            abs(float(a["ReflectedApparentTemperature"].split(" ", 1)[0]) - b["RTemp"])
            < 0.01
        ), "Reflected apparent temperature different"
        assert (
            abs(float(a["IRWindowTemperature"].split(" ", 1)[0]) - b["IRWTemp"]) < 0.01
        ), "IRWindowTemperature different"
        assert (
            abs(a["IRWindowTransmission"] - b["IRT"]) < 0.01
        ), "IRWindowTemperature different"
        assert (
            abs(float(a["RelativeHumidity"].split(" ", 1)[0]) - b["RH"]) < 0.01
        ), "Relative humidity different"
        assert abs(a["PlanckR1"] - b["PR1"]) < 0.01, "PlanckR1 different"
        assert abs(a["PlanckB"] - b["PB"]) < 0.01, "PlanckB different"
        assert abs(a["PlanckF"] - b["PF"]) < 0.01, "PlanckF different"
        assert abs(a["PlanckO"] - b["PO"]) < 0.01, "PlanckO different"
        assert abs(a["PlanckR2"] - b["PR2"]) < 0.01, "PlanckR2 different"

        return True
    except AssertionError as e:
        # print(e)
        # from pprint import pprint
        # pprint(a)
        # pprint(b)
        # input("Continue?")

        return False
    except Exception:
        # Other errors are considered ok (for example key errors)
        return True


def define_test_subjects(flir_samples_dir: Optional[str]) -> List[str]:
    return (
        flir_samples(flir_samples_dir)
        if flir_samples_dir
        else [
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e4_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e4_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e4_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e5_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e6_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e6_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e6_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e8xt_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e8xt_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e8xt_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e40_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e53_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e53_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e53_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e60bx_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e60bx_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e60bx_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e75_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e75_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_e75_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t630sc_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t630sc_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t630sc_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_sc660_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t660_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t660_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_t660_3.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_thermocam_b400_1.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_thermocam_b400_2.jpg",
            "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_thermocam_b400_3.jpg",
            # "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_one_1.jpg",
            # "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_one_2.jpg",
            # "https://bitbucket.org/nimmerwoner/flyr/downloads/flir_one_3.jpg",
        ]
    )


def flir_samples(flir_samples_dir: str):
    return [
        os.path.join(flir_samples_dir, sample)
        for sample in [
            "Air plane.jpg",
            "Car.jpg",
            "Disneyland.jpg",
            "IR_0007.jpg",
            "IR_0020.jpg",
            "IR_0026.jpg",
            "IR_0028.jpg",
            "IR_0030.jpg",
            "IR_0032.jpg",
            "IR_0034.jpg",
            "IR_0036.jpg",
            "IR_0044.jpg",
            "IR_0047.jpg",
            "IR_0048.jpg",
            "IR_0200.jpg",
            "IR_0269.jpg",
            "IR_0581.jpg",
            "IR_0636.jpg",
            "IR_2009-04-27_0007.jpg",
            "IR_P640-7 Deg.jpg",
            "Jet engine.jpg",
            "Petronas Twin Towers.jpg",
            "Statue.jpg",
            "Volcano.jpg",
            "Wasp nest.jpg",
            "building/Apartments.jpg",
            "building/Church.jpg",
            "building/Missing Insulation.jpg",
            "electrical/Fans.jpg",
            "electrical/Power lines panorama.jpg",
            "electrical/Radiators.jpg",
            "electrical/Solar panels.jpg",
            "electrical/Steel ladle.jpg",
            "Multi Spectral/Building/Cold air exfiltrating from indoors on hot day.jpg",
            "Multi Spectral/Building/Hot water pipe.jpg",
            "Multi Spectral/Building/Ladder to roof.jpg",
            "Multi Spectral/Building/Missing insulation on hot day.jpg",
            "Multi Spectral/Building/Pipe in wall.jpg",
            "Multi Spectral/Building/Shakes and window.jpg",
            "Multi Spectral/Building/Siding and window.jpg",
            "Multi Spectral/Building/Water heater.jpg",
            "Multi Spectral/Building/Wet fireplace bricks.jpg",
            "Multi Spectral/Electrical/Air compressor motor and belt.jpg",
            "Multi Spectral/Electrical/Air compressor motor.jpg",
            "Multi Spectral/Electrical/Fuses.jpg",
            "Multi Spectral/Electrical/Indoor transformer.jpg",
            "Multi Spectral/Electrical/Motors with gauges.jpg",
            "Multi Spectral/Electrical/Panel.jpg",
            "Multi Spectral/Electrical/Pump motors with gauges.jpg",
            "Panorama/Attic/IR_0136.jpg",
            "Panorama/Attic/IR_0137.jpg",
            "Panorama/Attic/IR_0138.jpg",
            "Panorama/House/IR_062603_019.jpg",
            "Panorama/House/IR_062603_021.jpg",
            "Panorama/House/IR_062603_023.jpg",
            "Panorama/House (Side)/IR_0121.jpg",
            "Panorama/House (Side)/IR_0122.jpg",
            "Panorama/Houses/IR_0018.jpg",
            "Panorama/Houses/IR_0019.jpg",
            "Panorama/Houses/IR_0117.jpg",
            "Panorama/Houses/IR_0118.jpg",
            "Panorama/Houses/IR_0119.jpg",
            "Panorama/Houses/IR_0124.jpg",
            "Panorama/Houses/IR_0125.jpg",
            "Panorama/Houses/IR_0126.jpg",
            "Panorama/Houses/IR_0130.jpg",
            "Panorama/Houses/IR_0131.jpg",
            "Panorama/Roof Leak/IR_0106.jpg",
            "Panorama/Roof Leak/IR_0107.jpg",
            "Panorama/Roof Leak/IR_0108.jpg",
            "Panorama/Roof Leak/IR_0109.jpg",
            "Panorama/Roof Leak/IR_0110.jpg",
            "Panorama/Roof Leak/IR_0111.jpg",
            "Panorama/Substation 2/IR_0231.jpg",
            "Panorama/Substation 2/IR_0232.jpg",
            "Panorama/Substation 2/IR_0233.jpg",
            "Panorama/Substation 2/IR_0234.jpg",
            "Panorama/Substation 2/IR_0235.jpg",
            "Panorama/Substation 2/IR_0236.jpg",
            "Panorama/Substation 2/IR_0237.jpg",
            "Panorama/Substation 2/IR_0238.jpg",
            "Panorama/Substation 2/IR_0239.jpg",
            "Panorama/Substation 2/IR_0240.jpg",
            "Panorama/Substation 2/IR_0241.jpg",
            "Panorama/Substation 2/IR_0242.jpg",
            "Panorama/Substation 2/IR_0243.jpg",
            "Panorama/Substation 2/IR_0244.jpg",
            "Panorama/Substation 2/IR_0245.jpg",
            "Panorama/Substation 2/IR_0246.jpg",
            "Panorama/Substation 2/IR_0247.jpg",
            "Panorama/Substation/Electrical_1.jpg",
            "Panorama/Substation/Electrical_2.jpg",
            "Panorama/Substation/Electrical_3.jpg",
            "Panorama/Substation/Electrical_4.jpg",
            "Panorama/Tower/IR_052903_059.jpg",
            "Panorama/Tower/IR_052903_060.jpg",
            "Panorama/Tower/IR_052903_061.jpg",
            "Panorama/Transformer/IR_061303_031.jpg",
            "Panorama/Transformer/IR_061303_032.jpg",
            "Panorama/Water Damage/Leakage1_1.jpg",
            "Panorama/Water Damage/Leakage1_2.jpg",
            "Panorama/Water Damage/Leakage1_3.jpg",
            "Panorama/Water Damage/Leakage1_4.jpg",
            "science/Engine.jpg",
            "science/Etna Luglio 2006.jpg",
            "science/Plane.jpg",
            "science/Test.jpg",
        ]
    ]


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "--dir",
        help="""
Directory where test thermograms are downloaded to / loaded from. Only works
with predefined thermograms. To test with custom files, use --thermogram
""",
        default="/tmp",
    )
    parser.add_argument(
        "--thermogram", help="Path to specific file to test", default=None
    )
    parser.add_argument(
        "--flir_samples",
        help="Path to the directory with the samples that come with FLIR Tools",
        default=None,
    )
    args = parser.parse_args()

    go()
