from io import BytesIO
from typing import List, Tuple

from flyr.flyr import extract_flir_app1, parse_flir_records
from flyr.records import RecordIndex
import flyr.measurement_info as mi

import pytest

checks = [
    ("thermograms/dji_xt2_1.jpg", 0),
    ("thermograms/dji_xt2_2.jpg", 0),
    ("thermograms/flir_e4_1.jpg", 0),
    ("thermograms/flir_e5_1.jpg", 0),
    ("thermograms/flir_e5_2.jpg", 0),
    ("thermograms/flir_e5_2-pip.jpg", 1),
    ("thermograms/flir_e6_1.jpg", 0),
    ("thermograms/flir_e8_wifi_1.jpg", 0),
    ("thermograms/flir_e8xt_wifi_1.jpg", 0),
    ("thermograms/flir_e75_1.jpg", 0),
    ("thermograms/flir_e40_1.jpg", 2),
    ("thermograms/flir_e50_1.jpg", 3),
    ("thermograms/flir_e50_2.jpg", 4),
    ("thermograms/flir_e50_3.jpg", 8),
    ("thermograms/flir_e50bx_1.jpg", 2),
    ("thermograms/flir_e50bx_2.jpg", 2),
    ("thermograms/flir_e50bx_3.jpg", 2),
    ("thermograms/flir_e60_1.jpg", 0),
    ("thermograms/flir_e60bx_1.jpg", 1),
    ("thermograms/flir_one_pro_next_gen_1.jpg", 0),
    ("thermograms/flir_thermocam_b400_2.jpg", 1),
    ("thermograms/flir_sc660_1.jpg", 1),
    ("thermograms/flir_vue_pro_640_1.jpg", 0),
]


@pytest.mark.parametrize("info", checks)
def test_parse_measurement_info(info: Tuple[str, int]):
    (file, measurements) = info

    with open(file, "rb") as stream:
        flir_app1_stream = extract_flir_app1(stream)
        records = parse_flir_records(flir_app1_stream)
        stream.seek(0)
        record = records.get(RecordIndex.MEASUREMENT_INFO.value, None)
        if record is None:
            return

        # check parsing
        measurement_infos = mi.parse_measurements(flir_app1_stream, record)
        assert measurements == len(measurement_infos)


_measurement_infos = [
    (mi.Tool.NONE, [], "", ""),
    (
        mi.Tool.SPOT,
        [80, 60],
        "1",
        "000001040004000000010001000000010002020100400000000000000000000000000050003c0031000000",
    ),
    (
        mi.Tool.SPOT,
        [161, 125],
        "1",
        "0000010400040000000100010000000100020201004000000000000000000000000000a1007d0031000000",
    ),
    (
        mi.Tool.AREA,
        [210, 157, 219, 166],
        "1",
        "0000010800040000000200010000000100540202004000000000000000000000000000d2009d00db00a6003100000000",
    ),
    (
        mi.Tool.ELLIPSE,
        [241, 29, 250, 29, 241, 20],
        "El1",
        "0000010c00080038000300010000000000840000000000000000000000000000000000f1001d00fa001d00f100140045006c003100000001001800000200000100010000ffff0000000000ffffe10005000c00e800140013001300000000000000000000",
    ),
    (
        mi.Tool.UNUSED,
        [51, 37778],
        "䌱",
        "00000104000400000007000100000001000000010040000000000000000000000000003393924331000000",
    ),
    (
        mi.Tool.UNUSED,
        [51, 37778],
        "䌱",
        "00000104000400000007000100000001000000010040000000000000000000000000003393924331000000",
    ),
]


@pytest.mark.parametrize("spot", _measurement_infos)
def test_parse_measurement(spot: Tuple[int, List[int], str, str]):
    (ref_tool_id, ref_params, ref_label, hex) = spot

    bs = BytesIO(bytes.fromhex(hex))
    bs.seek(0)
    measurement = mi.parse_measurement(bs)
    assert measurement is not None
    assert ref_tool_id == measurement.tool
    assert ref_params == measurement.params
    assert ref_label == measurement.label
