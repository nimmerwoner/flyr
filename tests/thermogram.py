import os
import math

import PIL
import flyr
import pytest
import numpy as np

import definitions as defs


@pytest.mark.parametrize("thermogram", defs.passing_unpacked, ids=defs.passing)
def test_thermal_properties(thermogram: flyr.FlyrThermogram):
    k = thermogram.kelvin
    c = thermogram.celsius
    f = thermogram.fahrenheit
    assert k is not c
    assert k is not f
    assert c is not f
    assert isinstance(k, np.ndarray)
    assert len(k.shape) == 2
    assert isinstance(c, np.ndarray)
    assert len(c.shape) == 2
    assert isinstance(f, np.ndarray)
    assert len(f.shape) == 2
    assert (k != c).all()
    assert (k != f).all()
    assert (c != f).all()


@pytest.mark.parametrize("thermogram", defs.passing_unpacked, ids=defs.passing)
def test_optical_properties(thermogram: flyr.FlyrThermogram):
    o = thermogram.optical
    o_pil = thermogram.optical_pil
    assert isinstance(o, np.ndarray) or o is None
    assert isinstance(o_pil, PIL.Image.Image) or o_pil is None
    if isinstance(o, np.ndarray):
        assert o.shape[2] == 3
        assert len(o.shape) == 3


@pytest.mark.filterwarnings("ignore:No embedded palette detected")
@pytest.mark.parametrize("thermogram", defs.passing_unpacked, ids=defs.passing)
def test_renders(thermogram: flyr.FlyrThermogram):
    k = thermogram.kelvin
    c = thermogram.celsius
    f = thermogram.fahrenheit
    r = thermogram.render()
    r_pil = thermogram.render_pil(edge_emphasis=1.0)

    assert isinstance(r, np.ndarray)
    assert isinstance(r_pil, PIL.Image.Image)
    assert r.shape[0] == r_pil.height == k.shape[0] == c.shape[0] == f.shape[0]
    assert r.shape[1] == r_pil.width == k.shape[1] == c.shape[1] == f.shape[1]
    assert r.shape[2] == 3


__sample = defs.take_random_indices(2)


@pytest.mark.parametrize(
    "thermogram", defs.passing_unpacked[__sample], ids=defs.passing[__sample]
)
def test_masked_rendering(thermogram: flyr.FlyrThermogram):
    thermal = thermogram.kelvin
    half_height = thermal.shape[0] // 2
    mask = np.zeros(thermal.shape, dtype=bool)
    mask[:half_height] = True
    embedded = thermogram.render(palette="embedded")
    grayscale = thermogram.render(palette="grayscale")
    masked = np.array(thermogram.render_pil(0.0, mask))
    thermogram.render_pil(0.0, mask)

    blur_offset = 2
    assert np.all(
        masked[: half_height - blur_offset] == embedded[: half_height - blur_offset]
    )
    assert np.all(
        masked[half_height + blur_offset :] == grayscale[half_height + blur_offset :]
    )


@pytest.mark.parametrize("thermogram", defs.passing_unpacked, ids=defs.passing)
def test_palettes(thermogram: flyr.FlyrThermogram):
    known_failures = [
        "flir_e40_1.jpg",
        "flir_sc660_2.jpg",
        "dji_xt2_1.jpg",
        "dji_xt2_2.jpg",
        "flir_vue_pro_640_1.jpg",
    ]
    if thermogram.identifier in known_failures:
        # TODO Bugfix that these files do pass
        assert thermogram.palette is None
        return

    assert isinstance(thermogram.palette, flyr.Palette)
    assert thermogram.palette.num_colors == len(thermogram.palette.yccs)
    assert thermogram.palette.num_colors == len(thermogram.palette.rgbs)
    assert isinstance(thermogram.palette.render(), np.ndarray)
    assert isinstance(thermogram.palette.render_pil(), PIL.Image.Image)


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def test_correct_embedded_range(t: flyr.FlyrThermogram):
    reference = {
        "flir_c5_1": ("celsius", 19.2, 37.3, 1),
        "flir_e4_1": ("celsius", -9.4, -1.1, 1),
        "flir_e4_2": ("fahrenheit", 65.5, 80.5, 1),
        "flir_e4_3": ("fahrenheit", 64.8, 79.8, 1),
        "flir_e5_1": ("celsius", 3.5, 11.2, 1),
        "flir_e5_2": ("fahrenheit", 80.7, 96.1, 1),
        "flir_e6_1": ("celsius", -7.4, 2.7, 1),
        "flir_e6_2": ("celsius", 6.1, 11.2, 1),
        "flir_e6_3": ("celsius", 6.3, 15.9, 1),
        "flir_e8_wifi_1": ("celsius", 3.6, 7.7, 1),
        "flir_e8_wifi_2": ("celsius", 4.1, 8.2, 1),
        "flir_e8_wifi_3": ("celsius", -5.2, 3.3, 1),
        "flir_e8xt_1": ("celsius", 20.6, 33.8, 1),
        "flir_e8xt_2": ("celsius", -3.5, 8.0, 1),
        "flir_e30_1": ("fahrenheit", 74.0, 98.0, 1),
        "flir_e30bx_1": ("celsius", 16.5, 36.6, 1),
        "flir_e40_1": ("celsius", 0.0, 35.0, 1),
        "flir_e40_2": ("fahrenheit", 64, 69, 0),
        "flir_e40_3": ("fahrenheit", 59, 66, 0),
        "flir_e50_1": ("fahrenheit", 77.8, 116.2, 1),
        "flir_e50_2": ("fahrenheit", 89.7, 94.2, 1),
        # "flir_e50_3": ("fahrenheit", 58.6, 489.9, 1),  # max_v fails, is 490.9
        "flir_e50bx_1": ("celsius", 3.1, 10.2, 1),
        "flir_e50bx_2": ("celsius", 4.0, 11.1, 1),
        "flir_e50bx_3": ("celsius", 5.3, 10.4, 1),
        "flir_e60_1": ("fahrenheit", 34.8, 55.6, 1),
        "flir_e60_2": ("fahrenheit", 35.2, 52.4, 1),
        "flir_e60_3": ("fahrenheit", 28.1, 51.8, 1),
        "flir_e60bx_1": ("celsius", -8.0, -0.1, 1),
        "flir_e60bx_2": ("celsius", -8.0, -0.1, 1),
        "flir_e60bx_3": ("celsius", -8.0, -0.1, 1),
        "flir_e75_1": ("celsius", 1.3, 10.2, 1),
        "flir_e75_2": ("celsius", 9.2, 13.4, 1),
        "flir_e75_3": ("celsius", 4.6, 9.6, 1),
        "flir_i5_1": ("celsius", 14, 22, 0),
        "flir_i5_2": ("celsius", -4, 34, 0),
        "flir_i5_3": ("celsius", 16, 36, 0),
        "flir_sc660_1": ("celsius", 22.9, 30.2, 1),
        "flir_sc660_2": ("celsius", 24.7, 46.1, 1),
        "flir_t630sc_1": ("celsius", -3.4, 14.2, 1),
        "flir_t630sc_2": ("celsius", 0.9, 15.0, 1),
        "flir_t630sc_3": ("celsius", 0.5, 15.0, 1),
        "flir_thermocam_b400_1": ("celsius", -3.8, 2.8, 1),
        "flir_thermocam_b400_2": ("celsius", -3.7, 2.9, 1),
        "flir_thermocam_b400_3": ("celsius", -2.5, 3.5, 1),
    }

    try:
        t_id: str = t.identifier.split(".")[0]  # type: ignore
    except:
        return
    if t_id not in reference:
        return

    ref_unit, ref_min_v, ref_max_v, ref_num_decs = reference[t_id]
    min_v, max_v = t.embedded_range(ref_unit)

    tolerance = 0.21  # TODO Further reduce tolerance
    assert math.isclose(round(min_v, ref_num_decs), ref_min_v, abs_tol=tolerance)
    assert math.isclose(round(max_v, ref_num_decs), ref_max_v, abs_tol=tolerance)


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def test_picture_in_picture(t: flyr.FlyrThermogram):
    if t.optical is None:
        try:
            t.picture_in_picture_pil()
            assert False, "Should fail due to missing optical imagery"
        except:
            pass
    else:
        pip = t.picture_in_picture_pil(edge_emphasis=0.275)
        assert isinstance(pip, PIL.Image.Image)
        render = t.render_pil()
        pip = t.picture_in_picture_pil(
            render=render, render_opacity=0.5, render_crop=False
        )
        assert isinstance(pip, PIL.Image.Image)

        # Not real tests, but at least ensures the code is executed
        mask = np.random.choice([False, True], size=t.kelvin.shape)
        _ = np.array(t.picture_in_picture_pil(mask=mask, mask_mode="alternative"))
        pip_class1 = t.picture_in_picture_pil(mask=mask, mask_mode="classical")
        pip_class2 = t.picture_in_picture_pil(mask=mask)
        assert np.all(pip_class1 == pip_class2)


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def embossment_masks(t: flyr.FlyrThermogram):
    if t.optical is None:
        try:
            t.picture_in_picture_pil()
            assert False, "Should fail due to missing optical imagery"
        except:
            pass
    else:
        e, o = t.embossment_masks()
        assert isinstance(e, np.ndarray)
        assert isinstance(o, np.ndarray)
        assert e.shape == o.shape

        e, o = t.embossment_masks_pil(opacity=1.0)
        assert isinstance(e, PIL.Image.Image)
        assert isinstance(o, PIL.Image.Image)


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def test_updating_metadata_in_place(t: flyr.FlyrThermogram):
    old_md = t.metadata
    old_temp = t.celsius

    new_emissivity = old_md.get("emissivity", 1.0) / 2
    new_object_distance = old_md.get("object_distance", 1.0) * 2

    updates = dict(emissivity=new_emissivity, object_distance=new_object_distance)
    new_t = t.adjust_metadata(in_place=True, **updates)
    new_md = t.metadata
    new_temp = t.celsius

    assert new_t is t
    assert new_md["emissivity"] == new_emissivity
    assert new_md["object_distance"] == new_object_distance
    assert np.all([new_md[k] == old_md[k] for k in old_md if k not in updates.keys()])
    assert (new_temp != old_temp).all()


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def test_updating_metadata_not_in_place(t: flyr.FlyrThermogram):
    old_md = t.metadata
    old_temp = t.celsius

    new_emissivity = old_md.get("emissivity", 1.0) / 2
    new_object_distance = old_md.get("object_distance", 1.0) * 2

    updates = dict(emissivity=new_emissivity, object_distance=new_object_distance)

    new_t1 = t.adjust_metadata(**updates)  # Test if default value works correctly
    new_t2 = t.adjust_metadata(in_place=False, **updates)

    new_md1 = new_t1.metadata
    new_md2 = new_t2.metadata
    new_temp1 = new_t1.celsius
    new_temp2 = new_t2.celsius

    assert new_t1 is not t
    assert new_t2 is not t
    assert new_t1 is not new_t2

    assert new_md1["emissivity"] == new_md2["emissivity"]
    assert new_md1["object_distance"] == new_md2["object_distance"]

    assert new_md1["emissivity"] == new_emissivity
    assert new_md2["emissivity"] == new_emissivity
    assert new_md1["object_distance"] == new_object_distance
    assert new_md2["object_distance"] == new_object_distance
    assert np.all(
        [
            new_md1[k] == old_md[k] and new_md2[k] == old_md[k]
            for k in old_md
            if k not in updates.keys()
        ]
    )
    assert (new_temp1 != old_temp).all()
    assert (new_temp2 != old_temp).all()


@pytest.mark.parametrize("t", defs.passing_unpacked, ids=defs.passing)
def test_measurement_info_presence(t: flyr.FlyrThermogram):
    assert isinstance(t.measurements, list)


def test_new_thermogram_creation_metadata():
    updates = {}
    thermal = np.random.rand(240, 320)
    t1 = flyr.FlyrThermogram(thermal=thermal, metadata={}, optical=None)
    t2 = t1.adjust_metadata(in_place=False, **updates)

    assert t1.optical is None
    assert t2.optical is None

    optical = np.random.rand(240, 320, 3)
    t1 = flyr.FlyrThermogram(thermal=thermal, metadata={}, optical=optical)
    t2 = t1.adjust_metadata(in_place=False, **updates)

    assert isinstance(t1.optical, np.ndarray)
    assert isinstance(t2.optical, np.ndarray)
    assert t1.optical.shape == (240, 320, 3)
    assert t2.optical.shape == (240, 320, 3)
    assert t1.optical is not t2.optical
    # assert t1.kelvin is not t2.kelvin  # Should be re-enabled when we provide metadata
    # assert t1.celsius is not t2.celsius
    # assert t1.metadata is not t2.metadata


def test_reference_safety():
    file = defs.passing[0]
    thermogram = flyr.unpack(os.path.join("thermograms", file))

    assert thermogram.celsius is not thermogram.celsius
    assert thermogram.kelvin is not thermogram.kelvin
    assert thermogram.metadata is not thermogram.metadata
    assert thermogram.optical is not thermogram.optical
