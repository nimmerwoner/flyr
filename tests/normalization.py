import numpy as np
import pytest

import flyr.normalization as norm


def test_minmax():
    height, width = 320, 240
    thermal = np.random.rand(height, width)
    min_v = thermal.min()
    max_v = thermal.max()

    normed = norm.by_minmax(min_v, max_v, thermal)
    manual = (thermal - min_v) / (max_v - min_v)

    assert normed.dtype in [float, np.float32, np.float64]
    assert normed.shape == thermal.shape
    assert (0.0 <= normed).all()
    assert (normed <= 1.0).all()
    assert (normed == manual).all()


def test_minmax_fail():
    height, width = 10, 10
    thermal = np.random.rand(height, width)
    try:
        normed = norm.by_minmax(np.mean(thermal), np.mean(thermal), thermal)
        assert False, "Minimum and maximum value should be different"
    except AssertionError:
        pass

    try:
        normed = norm.by_minmax(5.0, 3.0, thermal)
        assert False, "Minimum value should be smaller than maximum value"
    except AssertionError:
        pass


def test_percentiles():
    height, width = 320, 240
    thermal = np.random.rand(height, width)
    normed = norm.by_percentiles(0.0, 1.0, thermal)
    normed = norm.by_percentiles(0.1, 0.9, thermal)
    normed = norm.by_percentiles(0.2, 0.8, thermal)
    normed = norm.by_percentiles(0.3, 0.7, thermal)
    normed = norm.by_percentiles(0.4, 0.6, thermal)
    normed = norm.by_percentiles(0.45, 0.55, thermal)

    assert normed.dtype in [float, np.float32, np.float64]
    assert normed.shape == thermal.shape
    assert (0.0 <= normed).all()
    assert (normed <= 1.0).all()


def test_percentiles_fail():
    height, width = 10, 10
    thermal = np.random.rand(height, width)
    try:
        normed = norm.by_percentiles(0.9, 0.1, thermal)
        assert False, "Minimum percentile should be smaller than maximum"
    except AssertionError:
        pass

    try:
        normed = norm.by_percentiles(-0.1, 0.1, thermal)
        assert False, "Minimum percentile should be greater than 0.0"
    except AssertionError:
        pass

    try:
        normed = norm.by_percentiles(0.1, 1.1, thermal)
        assert False, "Maximum percentile should be smaller than 1.0"
    except AssertionError:
        pass
