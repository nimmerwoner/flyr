import os

import flyr
import pytest
import numpy as np

import definitions as defs


@pytest.mark.parametrize("thermogram", defs.passing_unpacked, ids=defs.passing)
def test_file_extraction(thermogram: flyr.FlyrThermogram):
    assert isinstance(thermogram, flyr.FlyrThermogram)


@pytest.mark.parametrize("filename", defs.failing_metadata)
def test_invalid_metadata(filename: str):
    f = os.path.join("thermograms", filename)
    assert os.path.isfile(f)
    thermogram = flyr.unpack(f)  # Extraction should not fail
    try:
        thermogram.kelvin
    except:
        return
    assert False, f"Thermal calculation {f} should fail"


@pytest.mark.parametrize("filename", defs.failing_tiff)
def test_file_extraction_failure_tiff(filename: str):
    f = os.path.join("thermograms", filename)
    assert os.path.isfile(f)
    try:
        thermogram = flyr.unpack(f)
    except:
        return
    assert False, f"Extracting {f} should fail"


@pytest.mark.parametrize("filename", defs.failing_inconsistent_chunks)
def test_file_extraction_failure_chunks(filename: str):
    f = os.path.join("thermograms", filename)
    assert os.path.isfile(f)
    try:
        thermogram = flyr.unpack(f)
    except:
        return
    assert False, f"Extracting {f} should fail"
