from typing import List
import os

import numpy as np

import flyr

passing = np.array(
    [
        "flir_c5_1.jpg",
        "flir_e4_1.jpg",
        "flir_e4_2.jpg",
        "flir_e4_3.jpg",
        "flir_e5_1.jpg",
        "flir_e5_2.jpg",
        "flir_e5_2-pip.jpg",
        "flir_e6_1.jpg",
        "flir_e6_2.jpg",
        "flir_e6_3.jpg",
        "flir_e8_wifi_1.jpg",
        "flir_e8_wifi_2.jpg",
        "flir_e8_wifi_3.jpg",
        "flir_e8xt_wifi_1.jpg",
        "flir_e8xt_wifi_2.jpg",
        "flir_e8xt_wifi_3.jpg",
        "flir_e40_1.jpg",
        "flir_e50_1.jpg",
        "flir_e50_2.jpg",
        "flir_e50_3.jpg",
        "flir_e30bx_1.jpg",
        "flir_e40_2.jpg",
        "flir_e40_3.jpg",
        "flir_e40_4.jpg",
        "flir_e50bx_1.jpg",
        "flir_e50bx_2.jpg",
        "flir_e50bx_3.jpg",
        "flir_e60_4.jpg",
        "flir_e60bx_1.jpg",
        "flir_e60bx_2.jpg",
        "flir_e60bx_3.jpg",
        "flir_e75_1.jpg",
        "flir_e75_2.jpg",
        "flir_e75_3.jpg",
        "flir_i5_1.jpg",
        "flir_i5_2.jpg",
        "flir_i5_3.jpg",
        "flir_one_1.jpg",
        "flir_one_g2_1.jpg",
        "flir_one_g2_2.jpg",
        "flir_one_g2_3.jpg",
        "flir_one_pro_1.jpg",
        "flir_one_pro_next_gen_1.jpg",
        "flir_one_pro_next_gen_2.jpg",
        "flir_t630sc_1.jpg",
        "flir_t630sc_2.jpg",
        "flir_t630sc_3.jpg",
        "flir_sc660_1.jpg",
        "flir_sc660_2.jpg",
        "flir_thermocam_b400_1.jpg",
        "flir_thermocam_b400_2.jpg",
        "flir_thermocam_b400_3.jpg",
        "flir_vue_pro_640_1.jpg",
        "dji_xt2_1.jpg",
        "dji_xt2_2.jpg",
    ]
)


passing_unpacked = np.array(
    [flyr.unpack(os.path.join("thermograms", f)) for f in passing]
)


failing_metadata: List[str] = []


failing_tiff: List[str] = []


failing_inconsistent_chunks: List[str] = []


def take_random_indices(num: int) -> np.ndarray:
    return np.random.choice(range(len(passing_unpacked)), num).astype(int)
