import os
from typing import Optional

import pytest
import definitions as defs


import flyr.camera_metadata as cm


__makes = {
    "flir_e40_1.jpg": "FLIR Systems",
    "flir_sc660_2.jpg": "FLIR Systems",
    "flir_vue_pro_640_1.jpg": "FLIR",
    "dji_xt2_1.jpg": "DJI",
    "dji_xt2_2.jpg": "DJI",
}

__models = {
    "flir_one_g2_1.jpg": "*",
    "flir_one_g2_2.jpg": "*",
    "flir_one_g2_3.jpg": "*",
    "flir_one_pro_next_gen_1.jpg": "",
    "flir_one_pro_next_gen_2.jpg": "",
    "flir_thermocam_b400_1.jpg": "thermacam b400",
    "flir_thermocam_b400_2.jpg": "thermacam b400",
    "flir_thermocam_b400_3.jpg": "thermacam b400",
    "flir_vue_pro_640_1.jpg": "vue pro 640 9mm",
    "dji_xt2_1.jpg": "xt2",
    "dji_xt2_2.jpg": "xt2",
}


@pytest.mark.parametrize("file", defs.passing)
def test_exif_parsing(file: str):
    path = os.path.join("thermograms", file)
    path_parts = file.split("_")
    md = cm.CameraMetadata(path)

    assert isinstance(md, cm.CameraMetadata)
    assert isinstance(md.make, str)
    assert md.make == __makes.get(file, "FLIR Systems AB")
    assert isinstance(md.model, str)
    assert md.model.lower() == __models.get(file, " ".join(path_parts[0:-1]))

    assert isinstance(md.data, dict)
    assert isinstance(md.gps_data, dict)
    assert isinstance(md.gps_latitude, float) or md.gps_latitude is None
    assert isinstance(md.gps_longitude, float) or md.gps_longitude is None
    assert isinstance(md.gps_altitude, float) or md.gps_altitude is None
    assert isinstance(md.gps_image_direction, float) or md.gps_image_direction is None
    assert isinstance(md.gps_map_datum, str) or md.gps_map_datum is None
