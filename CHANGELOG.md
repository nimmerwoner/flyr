Flyr ChangeLog
==============

## Upcoming

## 3.4.0 - 2024-12-14
- Exposed embedded measurements via `FlyrThermogram.measurements`.
  Specifically spot, ellipsis and area are tested for. Others such as line
  are expected to work too, but untested, due to lack of examples.

## 3.3.0 - 2023-03-13
- Added ability to access some EXIF metadata via `thermogram.camera_metadata`
  (GPS, date, camera).

## 3.2.0 - 2023-01-19
- Added ability to mask renders or mask picture-in-pictures, thermally
  highlighting only the masked region.
- Added ability to render to grayscale, but inverted using the name
  'grayscale-inverted'.

## 3.1.0 - 2022-02-12
- Added ability to render picture-in-pictures
- Added ability to emphasize edges in renders using the optical data
- Added options to the flyr executable for the above two features

## 3.0.0 - 2022-01-16
- Added extracting the palette embedded in the file.
- Default palette for `render()` is now the embedded palette.
- Added extracting the original render range embedded in the file.
- Default render range is now this range.
- Support for many more cameras, specifically those storing thermal data as raw
  data. Known models: SC660, ThermaCAM B400, E30, E30BX, E40, E50, E60BX.
- Package also installs the executable `flyr`.

## 2.0.0 - 2021-12-04
- Added Fahrenheit as unit to access temperature (`FlyrThermogram.fahrenheit`).
- Added unit to the signatures of `render` and `render_pil` to hint which unit
  the `min_v` and `max_v` parameters are in (only applicable in
  `mode='minmax'`).
- Changed `adjust_metadata` to be non-mutable by default. It returns a new
  thermogram instance instead of updating the old one in-place. The latter
  is still possible by passing `in_place=True`.
- Addition of `FlyrThermogram.optical_pil` propety to receive the optical data
  as a Pillow image.
- Expanded test coverage.

## 1.1.0 - 2021-04-13
- Allowed accessing the metadata dict through `FlyrThermogram.metadata`
- Introduced ability to change how temperatures are calculated based on the raw
  data. This is possible due to `FlyrThermogram.adjust_metadata`, with which
  parameters such as emissivity can be changed. This affects how the
  temperatures are calculated.
- Resolved bug where small thermograms fitting in only 1 chunk couldn't be
  read (affects for example FLIR C5).

## 1.0.0 - 2021-03-30
- Corrected rendering to non-grayscale
- Added palettes: civids, copper, gist\_earth, gist\_rainbow, hot, inferno,
  magma, ocean, plasma, rainbow, terrain, turbo, viridis
- Bumped to 1.0.0, because while not feature complete (more cameras to
  support, better error handling), it is in a good usable state.

## 0.4.6 - 2020-08-09
- Extracting the embedded image (or optical data as opposed to thermal data) is
  now supported as well.
- The FlyrThermogram objects sport a render() convenience function that renders
  the thermal data to a palette for a certain normalization.
- Fixed bug where Flyr got into endless loop unpacking certain files.
- No hard coded constants are used anymore for converting raw values into
  kelvin. Typically, the conversion differs only by about 0.1°C, sometimes
  less. However, the difference can also be as high as 0.6°C.
